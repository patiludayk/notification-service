package com.uday.stockmarketservice.utils.problemsolving;

import java.util.*;
import java.util.function.Consumer;

import lombok.Builder;

public class NamrataProblem1 {

    public static void main (String[] args) {

        String input = "Google 2, Facebook 3, Instagram 5, Google 3, Facebook 2, Instagram 2";
        Set<String> names = new HashSet<>();

        String[] split = input.split(", ");

        List<String> input1 = Arrays.asList(input.split(","));

        totalRating(input1, names::add);

        System.out.println(names);
    }

    public static void totalRating (List<String> input, Consumer<String> consumer) {

        input.forEach(in -> {
            in = in.trim();
            String[] value = in.split(" ");
            String name = value[0].trim();
            int rating = Integer.parseInt(value[1].trim());
            consumer.accept(name.trim());
            System.out.println(name + " " + rating);
        });
    }

    @Builder
    private static class Rating {
        String name;
        double rating;
    }

}
