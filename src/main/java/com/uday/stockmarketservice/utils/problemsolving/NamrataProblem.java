package com.uday.stockmarketservice.utils.problemsolving;

import java.util.*;

import lombok.Builder;

public class NamrataProblem {

    public static void main (String[] args) {

        List<Rating> input = new ArrayList<Rating>(Arrays.asList(Rating.builder().name("Google").rating(4.00).build(),
                                                                 Rating.builder().name("Facebook").rating(3.00).build(),
                                                                 Rating.builder().name("Instagram").rating(2.00).build(),
                                                                 Rating.builder().name("Google").rating(5.00).build(),
                                                                 Rating.builder().name("Facebook").rating(6.00).build(),
                                                                 Rating.builder().name("Instagram").rating(7.00).build(),
                                                                 Rating.builder().name("Google").rating(4.00).build(),
                                                                 Rating.builder().name("Facebook").rating(4.25).build(),
                                                                 Rating.builder().name("Instagram").rating(6.00).build()));

        HashMap<String, Map<Double, Double>> ratingMap = new HashMap<String, Map<Double, Double>>();

        for (Rating value : input) {

            if (ratingMap.get(value.name) != null) {
                HashMap<Double, Double> valueMap = (HashMap<Double, Double>) ratingMap.get(value.name);
                valueMap.put((double) (valueMap.keySet().size() + 1), valueMap.get((double) valueMap.keySet().size()) + value.rating);
                ratingMap.put(value.name, valueMap);
            }
            else {
                HashMap<Double, Double> valueMap = new HashMap<>();
                valueMap.put(1d, value.rating);
                ratingMap.put(value.name, valueMap);
            }

        }

        ratingMap.forEach( (key, value) -> {
            double size = value.keySet().size();
            Double sumValue = value.get(size);
            System.out.println(key + " " + sumValue / size);
        });

    }

    @Builder
    private static class Rating {
        String name;
        double rating;
    }

}
