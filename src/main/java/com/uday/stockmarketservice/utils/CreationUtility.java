package com.uday.stockmarketservice.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.uday.stockmarketservice.pojo.output.BestMatches;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

public class CreationUtility {

    private ObjectMapper objectMapper = new ObjectMapper();


    public <T> T getEntityFromFile(String fileName, Class<T> type) {
        File file = new File(fileName);
        T entity;
        try {
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            entity = objectMapper.readValue(file, type);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return entity;
    }


    //BestMatches bestMatches = getEntityFromFile("src/main/resources/data/BestMatche.json", BestMatches.class);
    BestMatches bestMatches = getEntityFromFile("src/main/resources/data/BestMatches.json", BestMatches.class);


    Gson GSON = new Gson();
    JsonParser PARSER = new JsonParser();
    String STOCK_KEY = "bestMatches";

    public List<Map<String, String>> parseJson(String json) {
        try {
            JsonElement jsonElement = PARSER.parse(json);
            JsonObject rootObject = jsonElement.getAsJsonObject();

            JsonElement errorMessage = rootObject.get("Error Message");
            if (errorMessage != null) {
                throw new RuntimeException(errorMessage.getAsString());
            }

            return resolve(rootObject);

        } catch (JsonSyntaxException e) {
            throw new RuntimeException("error parsing json", e);
        }
    }
    public List<Map<String, String>> resolve(JsonObject rootObject)  {
        Type bestMatches = new TypeToken<List<Map<String, String>>>() {
        }.getType();
        Type dataType = new TypeToken<Map<String, Map<String, String>>>() {
        }.getType();
        try {
            List<Map<String, String>> stockData = GSON.fromJson(rootObject.get("bestMatches"), bestMatches);
            return stockData;
            //return resolve(stockData);
        } catch (JsonSyntaxException e) {
            throw new RuntimeException("time series api change", e);
        }
    }

    /*public List<Map<String, String>> resolve(List<Map<String, String>> stockData)  {
        List<Map<String, String>> searchResult = null;
        try {
            stockData.forEach((key, values) -> searchResult.add(new HashMap<String, String>()
                    //.put(key, SIMPLE_DATE_FORMAT).atStartOfDay(),
                    .put("1. symbol", values),
                            .put(values.get("2. name")),
                    Double.parseDouble(values.get("3. type")),
                    Double.parseDouble(values.get("4. region")),
                    LocalDate.parse(values.get("5. marketOpen")),
                    LocalDate.parse(values.get("6. marketClose")),
                    Double.parseDouble(values.get("7. timezone")),
                    8. currency,
                    9. matchScore

            )));
        } catch (Exception e) {
            throw new AlphaVantageException("Weekly adjusted api change", e);
        }
        return new WeeklyAdjusted(metaData, stocks);
    }*/



    public static void main(String[] args) throws IOException {
        CreationUtility creationUtility = new CreationUtility();

        BestMatches bestMatches = creationUtility.bestMatches;

        //System.out.println(bestMatches.getBestMatches().get(0).getName());

        File resource = new File("src/main/resources/data/BestMatches.json");
        String data = new String(Files.readAllBytes(resource.toPath()));

        List<Map<String, String>> maps = creationUtility.parseJson(data);

        maps.stream().forEach( value ->{
            System.out.println(value.get("2. name"));
        });


    }

}
