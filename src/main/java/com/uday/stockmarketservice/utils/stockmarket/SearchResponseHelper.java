package com.uday.stockmarketservice.utils.stockmarket;

import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SearchResponseHelper {

    Logger logger = LoggerFactory.getLogger(SearchResponseHelper.class.getName());

    private Gson GSON = new Gson();
    private JsonParser PARSER = new JsonParser();

    public JsonObject parseJson(String json) {
        try {
            JsonElement jsonElement = PARSER.parse(json);
            JsonObject rootObject = jsonElement.getAsJsonObject();

            JsonElement errorMessage = rootObject.get("Error Message");
            if (errorMessage != null) {
                throw new RuntimeException(errorMessage.getAsString());
            }

            return rootObject;
        } catch (JsonSyntaxException e) {
            logger.error("Error parsing json", e);
            throw new RuntimeException("error parsing json", e);
        }
    }

    /*public List<Map<String, String>> parseJson(String json) {
        try {
            JsonElement jsonElement = PARSER.parse(json);
            JsonObject rootObject = jsonElement.getAsJsonObject();

            JsonElement errorMessage = rootObject.get("Error Message");
            if (errorMessage != null) {
                throw new RuntimeException(errorMessage.getAsString());
            }

            return resolve(rootObject);

        } catch (JsonSyntaxException e) {
            logger.error("Error parsing json", e);
            throw new RuntimeException("error parsing json", e);
        }
    }*/

    /*public List<Map<String, String>> resolve(JsonObject rootObject) {
        Type bestMatches = new TypeToken<List<Map<String, String>>>() {
        }.getType();
        Type dataType = new TypeToken<Map<String, Map<String, String>>>() {
        }.getType();
        try {
            List<Map<String, String>> stockData = GSON.fromJson(rootObject.get("bestMatches"), bestMatches);
            return stockData;
        } catch (JsonSyntaxException e) {
            logger.error("Cannot resolve jsonRootObject.", e);
            throw new RuntimeException("time series api change", e);
        }
    }*/

}
