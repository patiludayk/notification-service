package com.uday.stockmarketservice.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AlphaVantageClient {

    Logger logger = LoggerFactory.getLogger(AlphaVantageClient.class.getName());

    private static final String BASE_URL = "http://www.alphavantage.co/query?";
    @Value(value = "${com.uday.alphavantage.apikey}")
    private String apiKey;
    @Value(value = "${com.uday.alphavantage.timeout:3000}")
    private int timeOut;

    public String getRequest_old (Map<String, String> queryMap) {
        String query = getSearchQuery(queryMap);
        logger.info("request query : " + BASE_URL + query);
        try {
            URL request = new URL(BASE_URL + query);
            URLConnection connection = request.openConnection();
            connection.setConnectTimeout(timeOut);
            connection.setReadTimeout(timeOut);

            InputStreamReader inputStream = new InputStreamReader(connection.getInputStream(), "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStream);
            StringBuilder responseBuilder = new StringBuilder();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                responseBuilder.append(line);
            }
            bufferedReader.close();
            return responseBuilder.toString();
        }
        catch (IOException e) {
            logger.error("Error creating search request.", e);
            throw new RuntimeException("failure sending request", e);
        }
    }

    private OkHttpClient httpClient;

    private OkHttpClient defaultClient (int timeOut) {
        return new OkHttpClient.Builder().connectTimeout(timeOut, TimeUnit.SECONDS).build();
    }

    public String getRequest (Map<String, String> queryMap) {

        this.httpClient = this.httpClient == null ? defaultClient(timeOut) : httpClient;

        String query = getSearchQuery(queryMap);
        Request request = new Request.Builder().url(BASE_URL + query).build();

        logger.info("request query : " + BASE_URL + query);
        try {
            Call call = httpClient.newCall(request);
            Response response = call.execute();

            ResponseBody body = response.body();
            InputStreamReader inputStream = new InputStreamReader(body.byteStream(), "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStream);
            StringBuilder responseBuilder = new StringBuilder();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                responseBuilder.append(line);
            }
            bufferedReader.close();
            return responseBuilder.toString();
        }
        catch (IOException e) {
            logger.error("Error creating search request.", e);
            throw new RuntimeException("failure sending request", e);
        }
    }

    private String getSearchQuery (Map<String, String> queryMap) {
        StringBuilder urlBuilder = new StringBuilder();
        queryMap.forEach( (key, value) -> {
            urlBuilder.append("&" + key + "=" + value);
        });
        urlBuilder.append("&apikey=" + apiKey);
        return String.valueOf(urlBuilder);
    }

}
