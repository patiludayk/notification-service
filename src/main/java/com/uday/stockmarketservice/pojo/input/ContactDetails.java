package com.uday.stockmarketservice.pojo.input;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ContactDetails {
    private String name;
}
