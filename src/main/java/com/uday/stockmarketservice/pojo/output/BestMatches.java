package com.uday.stockmarketservice.pojo.output;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BestMatches {
    private String name;
    private List<StockDetail> bestMatches;
}