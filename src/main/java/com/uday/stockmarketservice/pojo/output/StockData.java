package com.uday.stockmarketservice.pojo.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class StockData {
    @JsonProperty
    private final LocalDateTime dateTime;
    @JsonProperty
    private final double open;
    @JsonProperty
    private final double high;
    @JsonProperty
    private final double low;
    @JsonProperty
    private final double close;
    @JsonProperty
    private final double adjustedClose;
    @JsonProperty
    private final long volume;
    @JsonProperty
    private final double dividendAmount;
    @JsonProperty
    private final double splitCoefficient;
}
