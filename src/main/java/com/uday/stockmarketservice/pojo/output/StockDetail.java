package com.uday.stockmarketservice.pojo.output;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StockDetail {

    private String symbol;
    private String name;
    private String type;
    private String region;
    private String marketOpen;
    private String marketClose;
    private String timezone;
    private String currency;
    private String matchScore;
}
