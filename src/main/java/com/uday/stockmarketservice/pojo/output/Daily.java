package com.uday.stockmarketservice.pojo.output;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class Daily {

    private Map<String, String> metadata;
    private final List<StockData> stockData;

}
