package com.uday.stockmarketservice.serviceimpl.stockmarket;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.uday.stockmarketservice.config.AlphaVantageClient;
import com.uday.stockmarketservice.pojo.output.BestMatches;
import com.uday.stockmarketservice.pojo.output.StockDetail;
import com.uday.stockmarketservice.utils.stockmarket.SearchResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class SearchServiceImpl {

    Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class.getName());

    private AlphaVantageClient alphaVantageClient;
    private SearchResponseHelper searchResponseHelper;

    private Gson GSON = new Gson();

    @Autowired
    public SearchServiceImpl(AlphaVantageClient alphaVantageClient, SearchResponseHelper searchResponseHelper) {
        this.alphaVantageClient = alphaVantageClient;
        this.searchResponseHelper = searchResponseHelper;
    }

    public BestMatches searchForSymbol(Map<String, String> queryMap) {
        String response = null;
        try {
            response = alphaVantageClient.getRequest(queryMap);
            logger.info("response received: " + response.substring(0, 10));
        } catch (Exception e) {
            logger.error("Error creating client.", e);
        }

        JsonObject searchResult = searchResponseHelper.parseJson(response);
        logger.info("Total stocks matching with keywords: " + searchResult.size());

        return resolve(searchResult);
    }

    private BestMatches resolve(JsonObject rootObject) {
        Type bestMatchesType = new TypeToken<List<Map<String, String>>>() {
        }.getType();
        try {
            List<Map<String, String>> stockData = GSON.fromJson(rootObject.get("bestMatches"), bestMatchesType);

            BestMatches bestMatches = BestMatches.builder().name("bestMatchesType").build();
            List<StockDetail> stockDetailList = new ArrayList<>();
            stockData.stream().forEach(r -> {
                StockDetail stockDetail = StockDetail.builder()
                        .symbol(r.get("1. symbol"))
                        .name(r.get("2. name"))
                        .type(r.get("3. type"))
                        .region(r.get("4. region"))
                        .marketOpen(r.get("5. marketOpen"))
                        .marketClose(r.get("6. marketClose"))
                        .timezone(r.get("7. timezone"))
                        .currency(r.get("8. currency"))
                        .matchScore(r.get("9. matchScore"))
                        .build();
                stockDetailList.add(stockDetail);
            });
            bestMatches.setBestMatches(stockDetailList);
            return bestMatches;
        } catch (JsonSyntaxException e) {
            logger.error("Cannot resolve jsonRootObject.", e);
            throw new RuntimeException("time series api change", e);
        }
    }
}
