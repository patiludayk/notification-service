package com.uday.stockmarketservice.serviceimpl.stockmarket;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.uday.stockmarketservice.config.AlphaVantageClient;
import com.uday.stockmarketservice.pojo.output.Daily;
import com.uday.stockmarketservice.pojo.output.StockData;
import com.uday.stockmarketservice.utils.stockmarket.SearchResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class DailyTimeSeriesServiceImpl {

    Logger logger = LoggerFactory.getLogger(DailyTimeSeriesServiceImpl.class.getName());
    private final DateTimeFormatter SIMPLE_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private AlphaVantageClient alphaVantageClient;
    private SearchResponseHelper searchResponseHelper;

    private Gson GSON = new Gson();

    @Autowired
    public DailyTimeSeriesServiceImpl (AlphaVantageClient alphaVantageClient, SearchResponseHelper searchResponseHelper) {
        this.alphaVantageClient = alphaVantageClient;
        this.searchResponseHelper = searchResponseHelper;
    }

    public Daily searchForDailyData(Map<String, String> queryMap) {

        String response = null;
        try {
            logger.info("Querymap : "+ queryMap);
            response = alphaVantageClient.getRequest(queryMap);
        }
        catch (Exception e) {
            logger.error("Error creating client.", e);
        }

        JsonObject searchResult = searchResponseHelper.parseJson(response);
        logger.info("resolve : "+ searchResult);
        return resolve(searchResult);
    }

    public Daily resolve (JsonObject rootObject) {
        Type metaDataType = new TypeToken<Map<String, String>>() {
        }.getType();
        Type dataType = new TypeToken<Map<String, Map<String, String>>>() {
        }.getType();
        try {
            Map<String, String> metaData = GSON.fromJson(rootObject.get("Meta Data"), metaDataType);
            Map<String, Map<String, String>> stockData = GSON.fromJson(rootObject.get("Time Series (Daily)"), dataType);

            List<StockData> stocks = new ArrayList<>();
            try {
                stockData.forEach( (key, values) -> {
                    stocks.add(StockData.builder()
                                        .dateTime(LocalDate.parse(key, SIMPLE_DATE_FORMAT).atStartOfDay())
                                        .open(Double.parseDouble(values.get("1. open")))
                                        .high(Double.parseDouble(values.get("2. high")))
                                        .low(Double.parseDouble(values.get("3. low")))
                                        .close(Double.parseDouble(values.get("4. close")))
                                        .volume(Long.parseLong(values.get("5. volume")))
                                        .build());
                });
            }
            catch (Exception e) {
                throw new RuntimeException("Daily api change", e);
            }
            return Daily.builder().metadata(metaData).stockData(stocks).build();
        }
        catch (JsonSyntaxException e) {
            throw new RuntimeException("time series api change", e);
        }
    }

}
