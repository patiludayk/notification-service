package com.uday.stockmarketservice.controller.stockmarket.search;

import com.uday.stockmarketservice.pojo.output.IntraDay;
import com.uday.stockmarketservice.service.stockmarket.IntraDayTimeSeriesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/intraday")
public class IntraDayController {

    Logger logger = LoggerFactory.getLogger(IntraDayController.class.getName());

    private IntraDayTimeSeriesService intraDayTimeSeriesService;

    @Autowired
    public IntraDayController (IntraDayTimeSeriesService intraDayTimeSeriesService) {
        this.intraDayTimeSeriesService = intraDayTimeSeriesService;
    }

    @GetMapping("/interval/{interval}")
    public IntraDay getIntraDayTimeSeriesData (@NotNull @PathVariable(value = "interval") String interval,
                                               @RequestParam(value = "outputsize", required = false) String outputSize) {
        IntraDay intraDay = IntraDay.builder().build();

        try {
            if (true) {
                IllegalArgumentException e = new IllegalArgumentException("This is error msg");
                throw e;
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return intraDay;
    }

}
