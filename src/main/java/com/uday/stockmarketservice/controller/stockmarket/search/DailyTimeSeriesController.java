package com.uday.stockmarketservice.controller.stockmarket.search;

import com.uday.stockmarketservice.pojo.output.Daily;
import com.uday.stockmarketservice.service.stockmarket.DailyTimeSeriesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/daily")
public class DailyTimeSeriesController {

    Logger logger = LoggerFactory.getLogger(DailyTimeSeriesController.class.getName());

    private DailyTimeSeriesService dailyTimeSeriesService;

    @Autowired
    public DailyTimeSeriesController (DailyTimeSeriesService dailyTimeSeriesService) {
        this.dailyTimeSeriesService = dailyTimeSeriesService;
    }

    @GetMapping("/compact/{symbol}")
    public Daily getCompactDailyTimeSeriesData (@NotNull @PathVariable(value = "symbol") String symbol) {
        Daily daily = Daily.builder().build();

        daily = dailyTimeSeriesService.getDailyCompactDataForShare(symbol);
        logger.info("daily: " + daily);
        return daily;
    }

    @GetMapping("/full/{symbol}")
    public Daily getFullDailyTimeSeriesData (@NotNull @PathVariable(value = "symbol") String symbol) {
        Daily daily = Daily.builder().build();

        daily = dailyTimeSeriesService.getDailyFullDataForShare(symbol);

        return daily;
    }

}
