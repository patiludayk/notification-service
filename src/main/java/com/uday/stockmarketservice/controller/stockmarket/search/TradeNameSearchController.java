package com.uday.stockmarketservice.controller.stockmarket.search;

import com.uday.stockmarketservice.pojo.output.BestMatches;
import com.uday.stockmarketservice.service.stockmarket.SearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/stock/")
public class TradeNameSearchController {

    Logger logger = LoggerFactory.getLogger(TradeNameSearchController.class.getName());

    SearchService searchService;

    @Autowired
    public TradeNameSearchController(SearchService searchService){
        this.searchService = searchService;
    }

    @GetMapping("/search/{keyword}")
    public BestMatches getMatchedShares (@NotNull @PathVariable(value = "keyword") String keyword) {
        BestMatches bestMatches = BestMatches.builder().build();
        try {
            bestMatches = searchService.searchUsingKeyword(keyword);
        }
        catch (Exception e) {
            logger.error("Something went wrong.", e);
            bestMatches.setName("Error from Controller...");
            return bestMatches;
        }
        return bestMatches;
    }

}
