package com.uday.stockmarketservice.controller;

import com.uday.stockmarketservice.pojo.input.ContactDetails;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/home/")
public class HomePage {

    @GetMapping("/")
    public String getHelloWorld() {
        return "Welcome....";
    }

    @PostMapping("/daily")
    public String getDailyTimeSeries(@RequestParam(value = "outputSize", required = true) String outputSize){

        return outputSize;
    }

    @PostMapping("/detail")
    public ContactDetails getContactDetails(@RequestBody ContactDetails contactDetails){

        return contactDetails;
    }

    @GetMapping("/testing")
    public ResponseEntity testingAPI(){

        return ResponseEntity.ok("testing api....");
    }

}
