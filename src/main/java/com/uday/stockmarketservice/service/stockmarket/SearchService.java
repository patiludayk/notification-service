package com.uday.stockmarketservice.service.stockmarket;

import com.uday.stockmarketservice.pojo.output.BestMatches;
import com.uday.stockmarketservice.serviceimpl.stockmarket.SearchServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class SearchService {

    Logger logger = LoggerFactory.getLogger(SearchService.class.getName());

    SearchServiceImpl searchServiceImpl;

    @Autowired
    public SearchService(SearchServiceImpl searchServiceImpl){
        this.searchServiceImpl = searchServiceImpl;
    }

    public BestMatches searchUsingKeyword(String keyword) {
        if (null != keyword && !keyword.isEmpty()){
            logger.info("keyword to search : " + keyword);
            Map<String, String> queryMap = new HashMap<>();
            queryMap.put("function", "SYMBOL_SEARCH");
            queryMap.put("keywords", keyword);
            return searchServiceImpl.searchForSymbol(queryMap);
        }
        return null;
    }
}
