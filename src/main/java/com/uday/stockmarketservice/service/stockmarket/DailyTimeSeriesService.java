package com.uday.stockmarketservice.service.stockmarket;

import java.util.HashMap;
import java.util.Map;

import com.uday.stockmarketservice.pojo.output.Daily;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uday.stockmarketservice.serviceimpl.stockmarket.DailyTimeSeriesServiceImpl;

@Service
public class DailyTimeSeriesService {

    Logger logger = LoggerFactory.getLogger(DailyTimeSeriesService.class.getName());

    private DailyTimeSeriesServiceImpl dailyTimeSeriesServiceImpl;

    @Autowired
    public DailyTimeSeriesService (DailyTimeSeriesServiceImpl dailyTimeSeriesServiceImpl) {
        this.dailyTimeSeriesServiceImpl = dailyTimeSeriesServiceImpl;
    }

    public Daily getDailyCompactDataForShare (String symbol) {

        if (!symbol.isEmpty()) {
            logger.info("Compact data for Trade : " + symbol);
            Map<String, String> queryMap = new HashMap<>();
            queryMap.put("function", "TIME_SERIES_DAILY");
            queryMap.put("symbol", symbol);
            queryMap.put("outputsize", "compact");
            return dailyTimeSeriesServiceImpl.searchForDailyData(queryMap);
        }
        return null;
    }

    public Daily getDailyFullDataForShare (String symbol) {
        if (!symbol.isEmpty()) {
            logger.info("Compact data for Trade : " + symbol);
            Map<String, String> queryMap = new HashMap<>();
            queryMap.put("function", "TIME_SERIES_DAILY");
            queryMap.put("symbol", symbol);
            queryMap.put("outputsize", "full");
            return dailyTimeSeriesServiceImpl.searchForDailyData(queryMap);
        }
        return null;
    }
}
