package com.uday.twitter.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uday.twitter.model.Keywords;
import com.uday.twitter.service.TwitterSearchByKeywordService;

@Controller
@RequestMapping("/api/user/twitter")
public class TwitterSearchByKeywordsController {

    Logger logger = LoggerFactory.getLogger(TwitterSearchByKeywordsController.class);

    @Autowired
    private TwitterSearchByKeywordService twitterSearchByKeywordService;

    private static int count = 0;

    @GetMapping("/searchtweets")
    public String getTweetSearchPage () {
        return "searchtweets";
    }

    @RequestMapping(path = "/search/by/keywords", consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE })
    public ResponseEntity searchTweetsByKeywordsStream (@NotNull Keywords keywords) {
        List<String> tweets = null;
        boolean tweetsStarted = twitterSearchByKeywordService.startProducingTweetsForUser(keywords);
        if (tweetsStarted) {
            tweets = twitterSearchByKeywordService.getTweets(keywords);
        }
        return ResponseEntity.ok(tweets);
    }

    @RequestMapping(path = "/search/by/keywords/polling", consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE })
    public ResponseEntity pollTweetsByKeywordsStream (@NotNull Keywords keywords) {
        List<String> tweets = null;
        logger.info("receive request for poll.");
        try {
            tweets = twitterSearchByKeywordService.getTweets(keywords);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        return ResponseEntity.ok(tweets);
    }

    @RequestMapping(path = "/search/by/stop", consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE })
    public ResponseEntity stopStream (@NotNull Keywords keywords) {
        boolean stopped = false;
        logger.info("stop request.");
        try {
            stopped = twitterSearchByKeywordService.stopTweets();
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        return ResponseEntity.ok(stopped);
    }

}
