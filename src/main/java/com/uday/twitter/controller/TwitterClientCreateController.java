package com.uday.twitter.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uday.twitter.model.TwitterDetails;
import com.uday.twitter.service.TwitterClientService;

import java.util.StringJoiner;

@Controller
@RequestMapping("/api/user/")
public class TwitterClientCreateController {

    Logger logger = LoggerFactory.getLogger(TwitterClientCreateController.class);

    @Autowired
    private TwitterClientService twitterClientService;

    @PostMapping("/twitter/secrets")
    public ResponseEntity registerTwitterClientForUser (TwitterDetails twitterDetails) {

        // store user specific twitter secrets to DB here
        int userSecrets = twitterClientService.saveTwitterSecrets(twitterDetails);
        if(userSecrets < 0){
            logger.error("Unable to save secrets! - " + userSecrets);
            return ResponseEntity.badRequest().body("Either User not exists or secrets already exists!");
        }
        return ResponseEntity.ok("user secrets saved!");
    }

    private void validateForNullAndEmptyValue (StringJoiner errorMsg, String field, String fieldLabel) {
        if (null == field || field.isEmpty()) {
            errorMsg.add(fieldLabel);
            logger.error("Error message. Input " + fieldLabel + " has wrong value - " + field);
        }
    }

}
