package com.uday.twitter.model;

import lombok.Data;

@Data
public class Keywords {
    private String keywords;
    private String user;
}
