package com.uday.twitter.model;

import java.util.concurrent.BlockingQueue;

import org.apache.kafka.clients.producer.KafkaProducer;

import com.twitter.hbc.core.Client;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientTweets {
    private Client client;
    private BlockingQueue<String> msgQueue;
    private KafkaProducer<String, String> producer;
}
