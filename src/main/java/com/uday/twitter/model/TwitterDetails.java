package com.uday.twitter.model;

import lombok.Data;

@Data
public class TwitterDetails {
    private String email;
    private String consumerKey;
    private String consumerSecret;
    private String  token;
    private String  secret;
}
