package com.uday.twitter.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uday.twitter.model.TwitterDetails;
import com.uday.twitter.serviceimpl.TwitterClientServiceImpl;

@Service
public class TwitterClientService {

    Logger logger = LoggerFactory.getLogger(TwitterClientService.class);

    @Autowired
    private TwitterClientServiceImpl twitterClientServiceImpl;

    public int saveTwitterSecrets(TwitterDetails twitterDetails) {
        return twitterClientServiceImpl.saveTwitterSecrets(twitterDetails);
    }
}
