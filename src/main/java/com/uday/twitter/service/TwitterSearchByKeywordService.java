package com.uday.twitter.service;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uday.twitter.model.Keywords;
import com.uday.twitter.serviceimpl.TwitterSearchByKeywordServiceImpl;

@Service
public class TwitterSearchByKeywordService {
    Logger logger = LoggerFactory.getLogger(TwitterSearchByKeywordService.class.getName());

    @Autowired
    private TwitterSearchByKeywordServiceImpl twitterSearchByKeywordServiceImpl;

    public boolean startProducingTweetsForUser(Keywords keywords) {
         //Set up your blocking queues: Be sure to size these properly based on expected TPS of your stream
        BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(1000);
        return twitterSearchByKeywordServiceImpl.createClientAndStartProducingTweetsToTopic(keywords, msgQueue);

    }

    public List<String> getTweets(Keywords keywords) {
        logger.info("polling for keywords..");
        return twitterSearchByKeywordServiceImpl.getTweetsForKeywords(keywords);
    }

    public boolean stopTweets() {
        logger.info("stop polling for.");
        return twitterSearchByKeywordServiceImpl.stopCLient();
    }
}
