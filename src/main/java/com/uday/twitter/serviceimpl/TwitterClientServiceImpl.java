package com.uday.twitter.serviceimpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uday.baseservice.utils.UserRepository;
import com.uday.twitter.model.TwitterDetails;
import com.uday.twitter.repository.TwitterRepository;

@Component
public class TwitterClientServiceImpl {

    Logger logger = LoggerFactory.getLogger(TwitterClientServiceImpl.class);

    @Autowired
    private TwitterRepository twitterRepository;

    @Autowired
    private UserRepository userRepository;

    public int saveTwitterSecrets (TwitterDetails twitterDetails) {
        boolean userExists = userRepository.findByEmail(twitterDetails.getEmail());
        boolean secretExists = userExists ? twitterRepository.userSecretsAvailable(twitterDetails.getEmail()) : true;
        if (userExists && !secretExists) {
            int saveSecrets = twitterRepository.saveSecrets(twitterDetails);
            return saveSecrets;
        }
        logger.error(userExists ? secretExists ? "Secrets already exists for - " + twitterDetails.getEmail() : "Secrets does not exists"
                    : "User does not exists");

        return -1; // means error or user already exists
    }
}
