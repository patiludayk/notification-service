package com.uday.baseservice.service;

import com.uday.baseservice.model.LoginDetails;
import com.uday.baseservice.model.RegistrationDetails;
import com.uday.baseservice.serviceimpl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserServiceImpl userServiceImpl;

    @Autowired
    public UserService (UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    Logger logger = LoggerFactory.getLogger(UserService.class.getName());

    public int registerUser (RegistrationDetails registrationDetails) {
        logger.info("registering user to service.");
        return userServiceImpl.registerUserToRecord(registrationDetails);
    }

    public boolean loginUser (LoginDetails loginDetails) {
        logger.info("login user to service.");
        return userServiceImpl.loginUser(loginDetails);
    }
}
