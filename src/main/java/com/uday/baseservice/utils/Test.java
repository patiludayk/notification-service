package com.uday.baseservice.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.ResourceUtils;

import com.uday.baseservice.model.Details;

public class Test {

    public static void main(String[] args) throws IOException {
        try {
            String path = "classpath:data" + File.separator + "userdetails.csv";
            File file = ResourceUtils.getFile(path);

            Details details = Details.builder().name("uday").age("30").mob("900").build();
            Details details1 = Details.builder().name("Rakesh").age("29").mob("700").build();
            Details details2 = Details.builder().name("Nikhil").age("29").mob("800").build();

            List<Details> products = new ArrayList<>();
            products.add(details);
            products.add(details1);
            products.add(details2);


        } catch (FileNotFoundException fileNotFoundException) {
            String path = "src/main/resources/data/userdetails.csv";
            File newFile = new File(path);
            try {
                new BufferedWriter(new FileWriter(newFile)).close();
            } catch (IOException e) {
                System.out.println("Error creating file." + e);
            }

        } catch (Exception e){
            System.out.println(e);
        } finally {
        }

        System.out.println("##############");
        System.out.println("{}");
        System.out.println("####################");
    }

}
