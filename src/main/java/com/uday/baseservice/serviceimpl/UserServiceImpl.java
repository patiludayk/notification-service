package com.uday.baseservice.serviceimpl;

import com.uday.baseservice.model.LoginDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uday.baseservice.model.RegistrationDetails;
import com.uday.baseservice.utils.UserRepository;

@Component
public class UserServiceImpl {

    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class.getName());

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public int registerUserToRecord (RegistrationDetails registrationDetails) {
        try {
            boolean userExists = userRepository.findByEmail(registrationDetails.getEmail());
            logger.info("User alredy exists: " + userExists);
            if (!userExists) {
                logger.info("Registering user to database: " + registrationDetails.getEmail());
                int savedUser = userRepository.save(registrationDetails);
                return savedUser;
            }
            logger.info("User not registered!");
            return -1;
            // TODO: handle error implementation pending - it should be UserRelatedException - with code and
            // message specific to user creation
        }
        catch (Exception e) {
            logger.error("Error registering user. " + e.getLocalizedMessage());
            return Integer.MIN_VALUE;
        }
    }

    public boolean loginUser (LoginDetails loginDetails) {
        // verify user details for twitter its basic details + twitter secrets and tokens
        boolean loginUser = userRepository.loginUser(loginDetails);
        logger.info("LoginUser : " + loginUser);
        boolean twitterDetailsExists = userRepository.findTwitterDetails(loginDetails.getEmail());
        logger.info("TwitterDetailsExists : " + twitterDetailsExists);
        logger.info("Allow to login: " + (loginUser && twitterDetailsExists));
        return (loginUser && twitterDetailsExists); // improve in future for message to register twitter secrete for twitter app don't
                                                     // reject login directly
    }

}

