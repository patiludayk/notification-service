package com.uday.baseservice.model;

import lombok.Builder;

@Builder
public class Details {
    String name;
    String age;
    String mob;
}
