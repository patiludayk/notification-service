package com.uday.baseservice.model;

import lombok.Data;

@Data
public class LoginDetails {
    private String email;
    private String password;
}
