package com.uday.baseservice.model;

import lombok.Data;

@Data
public class RegistrationDetails {
    private String name;
    private String email;
    private String mobile;
    private String password;
}
