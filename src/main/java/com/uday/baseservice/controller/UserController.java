package com.uday.baseservice.controller;

import java.util.StringJoiner;

import com.uday.baseservice.model.LoginDetails;
import com.uday.baseservice.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.uday.baseservice.model.RegistrationDetails;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/api/user/")
public class UserController {

    private UserService userService;

    Logger logger = LoggerFactory.getLogger(UserController.class.getName());

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/register")
    public String getRegisterPage () {
        return "register1";
    }

    @PostMapping(path = "/register", consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE })
    public ResponseEntity registerUser (@NotNull RegistrationDetails registrationDetails) {

        StringJoiner errorMsg = new StringJoiner(", ");
        StringJoiner responseMsg = validateUserDetails(registrationDetails, errorMsg);
        if (responseMsg.length() > 0) {
            return ResponseEntity.badRequest().body(responseMsg.toString());
        }
        int userName = userService.registerUser(registrationDetails);
        if(userName > 0){
            logger.info("User created!");
            responseMsg.add(registrationDetails.getEmail());
            return ResponseEntity.ok(responseMsg.toString());
        }
        return ResponseEntity.badRequest().body("Error registering user!");
    }

    /**
     * Validate User Details input
     * 
     * @param registrationDetails
     * @param errorMsg
     * @return List<String> list of error messages
     */
    private StringJoiner validateUserDetails (RegistrationDetails registrationDetails, StringJoiner errorMsg) {
        if (registrationDetails != null) {
            validateForNullAndEmptyValue(errorMsg, registrationDetails.getName(), "Name");
            validateForNullAndEmptyValue(errorMsg, registrationDetails.getEmail(), "Email");
            if (null == registrationDetails.getMobile() || registrationDetails.getMobile().length() != 10) {
                errorMsg.add("Invalid mobile number");
            }
        }
        return errorMsg;
    }

    private void validateForNullAndEmptyValue (StringJoiner errorMsg, String field, String fieldLabel) {
        if (null == field || field.isEmpty()) {
            errorMsg.add(fieldLabel);
            logger.error("Error message. Input " + fieldLabel + " has wrong value - " + field);
        }
    }

    @GetMapping("/twitterapp")
    public String getTwitterAppPage () {
        return "twitterapp";
    }

    @GetMapping("/login")
    public String getLoginPage () {
        return "login";
    }

    @PostMapping(path = "/login")
    public ResponseEntity loginUser (@NotNull LoginDetails loginDetails) {
        StringJoiner errorMsg = new StringJoiner(", ");
        validateForNullAndEmptyValue(errorMsg, loginDetails.getEmail(), "Email");
        validateForNullAndEmptyValue(errorMsg, loginDetails.getPassword(), "Password");
        if (errorMsg.length() > 0) {
            //return "errorPage";   //add error page
            logger.error("Bad credentials. Unable to login.");
            return ResponseEntity.badRequest().body("wrong username or password!");
        }

        boolean allowUserToLogin = userService.loginUser(loginDetails);
        if( !allowUserToLogin ){
            return ResponseEntity.badRequest().body("Email or Password is wrong! Please try again.");  //"errorPage";   //add error page
        }
        return ResponseEntity.ok("twitter/searchtweets");
    }

}
