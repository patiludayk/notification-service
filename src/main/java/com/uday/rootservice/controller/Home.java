package com.uday.rootservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Home {

    @RequestMapping("/")
    public String index() {
        return "welcome";
    }

    @RequestMapping("/start")
    public String start() {
        return "FirstInformationPage";
    }

}
