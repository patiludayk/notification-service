package com.uday.baseservice.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.uday.baseservice.model.RegistrationDetails;
import com.uday.baseservice.service.UserService;
import com.uday.baseservice.serviceimpl.UserServiceImpl;
import com.uday.baseservice.utils.UserRepository;
import com.uday.baseservice.utils.RepoClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { UserControllerTest.TestConfig.class })
public class UserControllerTest {

    @Autowired
    UserController userController;

    @InjectMocks
    UserService userService;

    @InjectMocks
    UserServiceImpl userServiceImpl;

    @InjectMocks
    UserRepository userRepository;

    @InjectMocks
    RepoClient repoClient;

    @Test
    public void registerUser_Test () {
        RegistrationDetails registrationDetails = new RegistrationDetails();
        registrationDetails.setName("nikhil");
        registrationDetails.setEmail("wow@wow.wow");
        registrationDetails.setMobile("9993334440");
        registrationDetails.setPassword("pass");

        ResponseEntity responseEntity = userController.registerUser(registrationDetails);

        String responseBody = (String) responseEntity.getBody();

        boolean userExists = responseBody.equals("user exists");
        boolean newUser = responseBody.equals("User created - uday");

    }

    @Configuration
    @ComponentScan(basePackages = { "com.uday" })
    public static class TestConfig {
    }

}
