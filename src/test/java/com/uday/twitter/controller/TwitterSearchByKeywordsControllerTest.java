package com.uday.twitter.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.uday.baseservice.model.RegistrationDetails;
import com.uday.baseservice.utils.UserRepository;
import com.uday.twitter.model.Keywords;
import com.uday.twitter.model.TwitterDetails;
import com.uday.twitter.repository.TwitterRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TwitterSearchByKeywordsControllerTest.TestConfig.class })
public class TwitterSearchByKeywordsControllerTest {

    @Autowired
    private TwitterSearchByKeywordsController twitterSearchByKeywordsController;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TwitterRepository twitterRepository;

    private Keywords keywords;

    @Before
    public void setUp () {
        keywords = getKeywords();
    }

    @Test
    public void searchTweetsByKeywordsStream_test () {

        try {
            ResponseEntity responseEntity = twitterSearchByKeywordsController.searchTweetsByKeywordsStream(keywords);
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    private Keywords getKeywords () {
        RegistrationDetails registrationDetails = new RegistrationDetails();
        registrationDetails.setName("nikhil");
        registrationDetails.setEmail("testmail");
        registrationDetails.setMobile("0000000000");
        registrationDetails.setPassword("pass");

        // delete user if exists - for tests
        if (userRepository.findByEmail(registrationDetails.getEmail())) {
            userRepository.delete(registrationDetails);
        }
        userRepository.save(registrationDetails);

        twitterRepository.deleteSecretsUsingUserId(userRepository.getUserId(registrationDetails.getEmail()));

        TwitterDetails twitterDetails = new TwitterDetails();
        twitterDetails.setEmail(registrationDetails.getEmail());
        twitterDetails.setConsumerKey("testconsumekey");
        twitterDetails.setConsumerSecret("testconsumersecrete");
        twitterDetails.setSecret("testsecrete");
        twitterDetails.setToken("testtoken");
        twitterRepository.saveSecrets(twitterDetails);

        Keywords keywords = new Keywords();
        keywords.setUser(twitterDetails.getEmail());
        keywords.setKeywords("test");
        return keywords;
    }

    @Test
    public void pollTweetsByKeywordsStream_test () {
        try {
            while(true) {
                ResponseEntity responseEntity = twitterSearchByKeywordsController.pollTweetsByKeywordsStream(keywords);
                System.out.println(responseEntity);
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    @Configuration
    @ComponentScan(basePackages = { "com.uday" })
    public static class TestConfig {
    }

}
