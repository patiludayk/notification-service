package com.uday.twitter.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.uday.baseservice.model.RegistrationDetails;
import com.uday.baseservice.utils.UserRepository;
import com.uday.twitter.model.TwitterDetails;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TwitterClientCreateControllerTest.TestConfig.class })
public class TwitterClientCreateControllerTest {

    @Autowired
    TwitterClientCreateController twitterClientCreateController;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    public void registerTwitterClientForUser_test(){

        TwitterDetails twitterDetails = new TwitterDetails();
        twitterDetails.setEmail("testemail");
        twitterDetails.setConsumerKey("testconsumekey");
        twitterDetails.setConsumerSecret("testconsumersecrete");
        twitterDetails.setSecret("testsecrete");
        twitterDetails.setToken("testtoken");

        RegistrationDetails registrationDetails = new RegistrationDetails();
        registrationDetails.setName("nikhil");
        registrationDetails.setEmail("testemail");
        registrationDetails.setMobile("9993334440");
        registrationDetails.setPassword("pass");
        userRepository.save(registrationDetails);

        twitterClientCreateController.registerTwitterClientForUser(twitterDetails);

        deleteuser("testemail");
    }

    private void deleteuser(String testmail) {
        jdbcTemplate.execute("DELETE FROM USERS WHERE email='" + testmail + "'");
    }


    @Configuration
    @ComponentScan(basePackages = { "com.uday" })
    public static class TestConfig {
    }
}
